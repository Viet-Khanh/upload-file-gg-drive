require('dotenv').config()

const {google}= require('googleapis')
const fs = require('fs')
const path = require('path')

const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const REDIRECT_URL = process.env.REDIRECT_URL
const REFRESH_URL = process.env.REFRESH_URL


const oauth2Client =new google.auth.OAuth2(CLIENT_ID , CLIENT_SECRET , REDIRECT_URL)

oauth2Client.setCredentials({refresh_token : REFRESH_URL})


const drive = google.drive({
    version : 'v3',
    auth : oauth2Client
})

const setFilePublic = async (fileId) => {
    try {
        await drive.permissions.create({
            fileId , 
            requestBody : {
                role : 'reader',
                type : 'anyone'
            }
        })

        const getUrl = await drive.files.get({
            fileId,
            fields : 'webViewLink , webContentLink'
        })
        return getUrl
    } catch (error) {
        
    }
}

const uploadFile = async ({shared}) => {
    try {
        const createFile = await drive.files.create(
            {
                requestBody : {
                    name : 'demo',
                    mimeType : 'image/jpg'
                },
                 media : {
                     mimeType : 'image/jpg',
                     body : fs.createReadStream(path.join(__dirname , '/../anh-meo-cute.jpeg'))
                 }
            }
        )
        const fileId = createFile.data.id
        const getUrl = await setFilePublic(fileId)
        console.log(getUrl.data)
    } catch (error) {
        console.log("err" ,error)
    }
}

const deleteFile = async (fileId) => {
    try {
        const delFile = await drive.files.delete({
            fileId : fileId
        })
        console.log(delFile.data)
    } catch (error) {
        console.log("err" ,error)
    }
}


module.exports = {
    setFilePublic,
    uploadFile ,
    deleteFile
}